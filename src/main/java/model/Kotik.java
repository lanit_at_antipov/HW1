package model;

public class Kotik {
    private double satietyCount = 2.0;
    private static int catCount = 0;
    private int prettiness;
    private String name;
    private int weight;
    private String meow;

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.catCount++;
        System.out.println("Инициализирован объект Kotik:" + "\n"
                + "Возраст: " + prettiness + "\n"
                + "Имя: " + name + "\n"
                + "Вес: " + weight + "\n"
                + "Тип мяу: " + meow + "\n");
    }

    public Kotik() {
        this.catCount++;
        System.out.println("Инициализирован объект безымянный Kotik. Задать праметры можно с помощью метода setKotik()" + "\n");
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public boolean play() {
        if (satietyCount > 0) {
            System.out.println("Вы играете с котиком");
            satietyCount--;
            return true;
        } else {
            System.out.println("Котик не готов сейчас играть, его нужно покормить. Уровень сытости " + satietyCount);
            return false;
        }
    }

    public boolean sleep() {
        if (satietyCount > 0) {
            System.out.println("Котик уснул");
            satietyCount -= 0.5;
            return true;
        } else {
            System.out.println("Котик не может уснуть, его нужно покормить. Уровень сытости " + satietyCount);
            return false;
        }
    }

    public boolean chaseMouse() {
        if (satietyCount > 0) {
            System.out.println("Котик увидел мышь и устроил погоню за ней");
            satietyCount -= 1.5;
            return true;
        } else {
            System.out.println("Котик увидел мышь, но ничего не делает, его нужно покормить. Уровень сытости " + satietyCount);
            return false;
        }
    }

    public boolean eat(Double portionsOfEat) {
        satietyCount += portionsOfEat;
        System.out.println("Котик поел, " + portionsOfEat + " порции(ий) корма, уровень сытости равен " + satietyCount);
        return true;
    }

    public boolean eat(Double portionsOfEat, String foodName) {
        satietyCount += portionsOfEat;
        System.out.println("Котик поел, " + portionsOfEat + " порции(ий) " + foodName + " , уровень сытости равен " + satietyCount);
        return true;
    }

    public boolean eat() {
        eat(1.5, "корма");
        return true;
    }

    public boolean meow() {
        if (satietyCount > 0) {
            satietyCount -= 0.1;
            System.out.println(meow);
            return true;
        } else {
            System.out.println("Вы гладите котика, но вместо мяуканья вы слышате урчание в животе у кота");
            return false;
        }
    }

    public boolean looksOutTheWindow() {
        if (satietyCount > 0) {
            satietyCount -= 0.5;
            System.out.println("Котик смотрит в окно");
            return true;
        } else {
            System.out.println("Котик хочет есть, а не вот это вот всё");
            return false;
        }
    }


    public boolean readyToAction() {
        if (satietyCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void liveAnotherDay() {
        for (int i = 1; i <= 24; i++) {
            int mathRandom = (int) (Math.random() * 5 + 1);
            switch (mathRandom) {
                case 1:
                    if (readyToAction() == true) {
                        play();
                    } else {
                        eat(5.0);
                    }
                    break;

                case 2:
                    if (readyToAction() == true) {
                        sleep();
                    } else {
                        eat(5.0);
                    }
                    break;

                case 3:
                    if (readyToAction() == true) {
                        chaseMouse();
                    } else {
                        eat(5.0);
                    }
                    break;

                case 4:
                    if (readyToAction() == true) {
                        meow();
                    } else {
                        eat(5.0);
                    }
                    break;
                case 5:
                    if (readyToAction() == true) {
                        looksOutTheWindow();
                    } else {
                        eat(5.0);
                    }
                    break;
            }
        }
    }

    public boolean getSatietyCount() {
        if (satietyCount > 0) {
            System.out.println("Котик хорошо себя чувствует, уровень сытости равен " + satietyCount);
            return true;
        } else {
            System.out.println("Котик не готов заниматься кошачьими делами, он готов только ЕСТЬ! Уровень сытости " + satietyCount);
            return false;
        }
    }


    public static int getCatCount() {
        return catCount;
    }


    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }
}