import model.Kotik;

public class Application {
    public static void main (String args []) {
        Kotik murzik = new Kotik(1, "Мурзик", 4, "Мяу");
        Kotik basya = new Kotik();
        basya.setKotik(2, "Бася", 2, "Муау");

        murzik.liveAnotherDay();
        System.out.println("");

        System.out.println("Имя котика прожившего ещё один день: " + murzik.getName() +
                ", его вес равен: " + murzik.getWeight()+ "\n");

        System.out.println("Результат сравнения одинаково ли разговаривают котики" );
        System.out.println(murzik.getMeow() == basya.getMeow());
        System.out.println("");

        System.out.println("Всего создано " + Kotik.getCatCount() + " котика");
    }
}
